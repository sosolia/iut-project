# **Node JS**

# iut-project

**Commande à savoir :**

`npm start` : démarrer le serveur.

![img.png](img.png)

**Installation des dépendances(librairies) si nécessaires :**

`npm i :` installe toutes les dépendances.

`npm i @hapi/hapi` : dépendances hapi.

`npm i --save schmervice` : dépendances schmervice.

`npm i --save @hapi/jwt :` dépendances Jwt(Json Web Token).

Ce n'est pas nécessaires si la première commande est faites.

`docker run --name hapi-mysql -e MYSQL_ROOT_PASSWORD=hapi -e MYSQL_DATABASE=user -d mysql:8`

Pour la base de donnée mysql ou bien passer tous simplement par wamp.

`Login :` root

`Password :` ' '

( il n'y a pas de pawword).

--------------------------------------------------------------

### **TP réalisés :**

### `TP - NPM`

### `TP - Hapi`

### `TP - Swagger`

### `TP - Validation`

### `TP - Objection et Schwifty`

### `TP - Schmervice`

### `TP - User`

### `TP - Authentication`


![img_1.png](img_1.png)
`Vérification du token.`

Insertion du Token dans `Authorize` en haut à droite, après authentification via la route login.

![img_2.png](img_2.png)

Le token sera fournis en réponse à la connexion.

**Ne pas oublier :**  de mettre  Bearer  avec un espace avanr le Token


----------------------------------------------------------------------------------
### `Projet`

- **Envoyer un mail de bienvenue** lorsqu'un utilisateur est créer
- Pensez a créer un service dédié a l'envoi de mail
- Pensez a utilisateur des variables d'environnement pour définir la connexion au serveur de mail
- Je vous conseil d'utiliser nodemailer pour envoyer vos mail
- Utiliser https://ethereal.email/ afin de tester vos mail

Migrations : 0-mail.js

Models : mail.js

Service : nodemailer.js

Route : send.js

L'envoie de mail n'est pas `fonctionnel`, je n'ai pas réussi à créer l'envoie de mail.
Pour compenser cette fonctionnaliter manquante, j'ai fait la `création de mail qui s'insère dans la base de donnée`.

-----------------------------------------------------------------

- Créer un bibliothèque de films
- Un film devra contenir au moins les informations suivants:
    - titre
    - description
    - date de sortie
    - réalisateur
- Veuillez a mettre une validation et des champs en base de données correct ainsi que les dates de création et modifications en base de données.
- Uniquement les utilisateurs admin peuvent créer, rajouter et modifier les films.
- Les utilisateurs (scope: user) doivent pouvoir gérer une liste de films favoris (a partir de la liste de films disponibles)
- Pensez a générer des messages d'erreurs approprié si l'utilisateur ajoute un film qu'il a déjà en favoris ou qu'il essaye de supprimer un film qui n'est pas dans ces favoris.

Migrations : 0-films.js

Models : film.js

Service : film.js

Routes :
- createFilm.js
- listFilm.js
- updateFilm.js
- deleteFilm.js

Toutes les fonctionnalitées demandées ont été faites et sont fonctionnelles.

----------------------------------------------------------------------------

- Notifications
    - Lorsqu'un nouveau film est ajouté nous aimerions pouvoir prévenir les utilisateurs par mail
    - Lorsqu'un film est modifier, les utilisateurs qui ont le film en favoris doivent être informé par mail.

Pour donner suite à l'utilisation de `nodemailer`, cette partie là ne fonctionne pas non plus et n'est pas présente dans ce projet.

_________________________________________________________________________________

                                            END

