'use strict';

const iut_encrypt = require('@fiau372/iut-encrypt');

const password = 'motdepasse';

const passwordSha1 = iut_encrypt.sha1(password);


if(iut_encrypt .compareSha1('motdepassesaisit', passwordSha1)){

    console.log('Connexion valide');
}