'use strict';

const Joi = require('joi')
//const transport = require("nodemailer");

module.exports = {
    method: 'post',
    path: '/mail',
    options: {
        auth: false,
        tags: ['api'],
        validate: {
            payload: Joi.object({
                from: Joi.string().min(3).example('john.doe@gmail.com').description('From the sender'),
                to: Joi.string().min(3).example('jane.doe@gmail.com').description('to the receiver'),
                subject: Joi.string().min(3).example('Jobs').description('subject of the email'),
                text: Joi.string().min(8).example('hello, i have a lot things to say').description('container for the message for the email'),
                signature: Joi.string().min(8).example('Doe John').description('signature of the email'),
                date: Joi.date().example('user').description('date of the email'),
            })
        }
    },
    handler: async (request) => {

        /*transport.createTransport({
            host: 'smtp.mailtrap.io',
            port: 2525,
        });*/
        const { mailService } = request.services();
        return await mailService.sendEmail(request.payload);
    }
};