'use strict';

const Joi = require('joi')

module.exports = {
    method: 'delete',
    path: '/DELETE/user/{id}',
    options: {
        auth : {
            scope: [ 'admin' ]
        },
        tags: ['api'],
        validate: {
            payload: Joi.object({
                id : Joi.number().integer().greater(0).example(4)
            })
        }
    },
    handler: async (request) => {
        const { userService } = request.services();
        try{
            userService.remove(request.payload);
        }catch (e) {
            return e;
        }
        //deleteUserService.delete(request.payload);

        return '';
    }
};