'use strict';

const Joi = require('joi')

module.exports = {
    method: 'patch',
    path: '/PATCH/user/{id}',
    options: {
        tags: ['api'],
        auth : {
            scope: [ 'admin' ]
        },
        validate: {
            payload: Joi.object({
                id : Joi.number().integer().greater(0).example(4),
                firstName: Joi.string().required().min(3).example('John').description('Firstname of the user'),
                lastName: Joi.string().required().min(3).example('Doe').description('Lastname of the user'),
                username: Joi.string().alphanum().min(3).max(30).required().example('johnD').description('username of the user'),
                email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'fr'] } }).min(8).example('john.doe@gmail.com').description('email of the user'),
                password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')).min(8).example('password1!').description('password of the user'),

            })
        }
    },

    handler: async (request) => {
        const { userService } = request.services();
        try{
            await userService.updateUser(request.payload);
        }catch (e) {
            return e;
        }

        return '';
    }

};