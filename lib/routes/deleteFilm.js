'use strict';

const Joi = require('joi')

module.exports = {
    method: 'delete',
    path: '/DELETE/film/{id}',
    options: {
        auth : {
            scope: [ 'admin' ]
        },
        tags: ['api'],
        validate: {
            payload: Joi.object({
                id : Joi.number().integer().greater(0).example(2)
            })
        }
    },
    handler: async (request) => {
        const { filmService } = request.services();
        try{
           await  filmService.removeFilm(request.payload);
        }catch (e) {
            return e;
        }

        return '';
    }
};