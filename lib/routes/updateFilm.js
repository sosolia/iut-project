'use strict';

const Joi = require('joi')

module.exports = {
    method: 'patch',
    path: '/PATCH/film/{id}',
    options: {
        tags: ['api'],
        auth : {
            scope: [ 'admin' ]
        },
        validate: {
            payload: Joi.object({
                id: Joi.number().integer().greater(0),
                titre: Joi.string().min(3).example('Riverdale').description('title of the film'),
                description: Joi.string().min(3).example('Série Fantastique').description('description of the film'),
                realisateur: Joi.string().min(3).example('Inconnue').description('director of the film'),
                favoris: Joi.string().min(3).example('Oui').description('favoris'),
                date_sortie: Joi.date()
            })
        }
    },

    handler: async (request) => {
        const {filmService} = request.services();
        try {
            await filmService.updateFilm(request.payload);
        } catch (e) {
            return e;
        }

        return '';
    }

};