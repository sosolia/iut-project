'use strict';

const Joi = require('joi')

module.exports = {
    method: 'patch',
    path: '/SCOPE/users',
    options: {
        auth : {
            scope: ['admin']
        },
        tags: ['api'],
        validate: {
            payload: Joi.object({
                id: Joi.number().integer().greater(0).example(3).description('id of the user')
            })
        }
    },
    handler: async (request) => {
        const { userService } = request.services();
        return await userService.scopeUser(request.payload);
    }
};