'use strict';

const Joi = require('joi')

module.exports = {
    method: 'post',
    path: '/POST/user/login',
    options: {
        auth: false,
        tags: ['api'],
        validate: {
            payload: Joi.object({
                //id : Joi.number().integer().greater(0).example(4),
                //username: Joi.string().alphanum().min(3).max(30).required().example('johnD').description('username of the user'),
                email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'fr'] } }).min(8).example('jerky.Joe@gmail.com').description('email of the user'),
                password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')).min(8).example('password').description('password of the user'),
            })
        }
    },

    handler: async (request) => {
        const { userService } = request.services();
        return await userService.login(request.payload);
    }

};