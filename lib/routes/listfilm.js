'use strict';

module.exports = {
    method: 'get',
    path: '/films',
    options: {
        auth : {
            scope: ['user']
        },
        tags: ['api'],
        validate: {}
    },
    handler: async (request) => {
        const {filmService} = request.services();
        return await filmService.listFilm();
    }
};