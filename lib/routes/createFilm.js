'use strict';

const Joi = require('joi')

module.exports = {
    method: 'post',
    path: '/film',
    options: {
        auth : {
            scope: ['admin']
        },
        tags: ['api'],
        validate: {
            payload: Joi.object({
                titre: Joi.string().min(3).example('Les septs merveilles du monde').description('title of the film'),
                description: Joi.string().min(3).example('Documentaire').description('description of the film'),
                realisateur: Joi.string().min(3).example('TF1').description('director of the film'),
                favoris: Joi.string().min(3).example('non').description('favoris'),
                date_sortie: Joi.date().example('2021-03-30 00:00:00')
            })

        }

    },

    handler: async (request) => {

        const {filmService} = request.services();
        return await filmService.createFilm(request.payload);
    }
};

