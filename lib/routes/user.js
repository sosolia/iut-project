'use strict';

const Joi = require('joi')

module.exports = {
    method: 'post',
    path: '/user',
    options: {
        auth: false,
        tags: ['api'],
        validate: {
            payload: Joi.object({
                firstName: Joi.string().required().min(3).example('John').description('Firstname of the user'),
                lastName: Joi.string().required().min(3).example('Doe').description('Lastname of the user'),
                username: Joi.string().alphanum().min(3).max(30).required().example('johnD').description('username of the user'),
                email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'fr'] } }).example('john.doe@gmail.com').description('email of the user'),
                password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')).min(3).example('password').description('password of the user'),

            })

        }



    },

    handler: async (request, h) => {

        const { userService } = request.services();
        return await userService.create(request.payload);
    }
};

