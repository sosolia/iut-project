'use strict';

module.exports = {
    method: 'get',
    path: '/users',
    options: {
        auth : {
            scope: ['admin', 'user']
        },
        tags: ['api'],
        validate: {}
    },
    handler: async (request) => {
        const { userService } = request.services();
        return await userService.list();
    }
};