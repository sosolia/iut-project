'use strict';

const Joi = require('joi');
const {Model} = require('schwifty');

module.exports = class Mail extends Model {

    static get tableName() {

        return 'mail';
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer().greater(0),
            from: Joi.string().min(3).example('john.doe@gmail.com').description('From the sender'),
            to: Joi.string().min(3).example('jane.doe@gmail.com').description('to the receiver'),
            subject: Joi.string().min(3).example('Jobs').description('subject of the email'),
            text: Joi.string().min(8).example('hello, i have a lot things to say').description('container for the message for the email'),
            signature: Joi.string().min(8).example('Doe John').description('signature of the email'),
            date: Joi.date().description('date of the email'),
            createdAt: Joi.date(),
            updatedAt: Joi.date()
        });
    }

    $beforeInsert(queryContext) {

        this.updatedAt = new Date();
        this.createdAt = this.updatedAt;
    }

    $beforeUpdate(opt, queryContext) {

        this.updatedAt = new Date();
    }

};