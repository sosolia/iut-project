'use strict';

const Joi = require('joi');
const {Model} = require('schwifty');

module.exports = class Film extends Model {

    static get tableName() {

        return 'film';
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer().greater(0),
            titre: Joi.string().min(3).example('John').description('title of the film'),
            description: Joi.string().min(3).example('Doe').description('description of the film'),
            realisateur: Joi.string().min(3).example('johny Depp').description('director of the film'),
            favoris: Joi.string().min(3).example('john.doe@gmail.com').description('favoris'),
            date_sortie: Joi.date(),
            createdAt: Joi.date(),
            updatedAt: Joi.date()
        });
    }


    $beforeInsert(queryContext) {

        this.updatedAt = new Date();
        this.createdAt = this.updatedAt;
    }

    $beforeUpdate(opt, queryContext) {

        this.updatedAt = new Date();
    }

};