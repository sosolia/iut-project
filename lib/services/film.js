const {Service} = require('schmervice');

module.exports = class FilmService extends Service {

    async createFilm(film) {
            const {Film} = this.server.models();
            return await Film.query().insertAndFetch(film);

    }

    listFilm() {
        const {Film} = this.server.models();
        return Film.query();
    }

    async updateFilm(film) {

        const {Film} = this.server.models();
        return await Film.query().updateAndFetchById(film.id, film);
    }

    async removeFilm(film) {

        const {Film} = this.server.models();
        await Film.query().deleteById(film.id);
        return 'Movie delete';

    }
}