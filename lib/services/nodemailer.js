'use strict';

const {Service} = require('schmervice');
//const transport = require("nodemailer");
module.exports = class mailService extends Service {

    async sendEmail(mail) {
        const {Mail} = this.server.models();
        //return await Mail.send(mail);
        return await Mail.query().insertAndFetch(mail);
    }

}