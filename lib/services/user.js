'use strict';

const {Service} = require('schmervice');

module.exports = class UserService extends Service {

    create(user) {
        const iut_encrypt = require('@fiau372/iut-encrypt');
        const {User} = this.server.models();
        user.password = iut_encrypt.sha1(user.password);
        return User.query().insertAndFetch(user);
    }

    list() {
        const {User} = this.server.models();
        return User.query();
    }

    async scopeUser(user) {
        const {User} = this.server.models();
        try {
            user.scope = 'admin';
            await User.query().updateAndFetchById(user.id, user);
            return 'scope: admin';

        } catch (e) {
            return e;
        }
        return 'ko';
    }

    async updateUser(user) {
        const {User} = this.server.models();
        const iut_encrypt = require('@fiau372/iut-encrypt');
        user.password = iut_encrypt.sha1(user.password);
        return await User.query().updateAndFetchById(user.id, user);

    }

    async remove(user) {
        const {User} = this.server.models();

        await User.query().deleteById(user.id);

        return '';
    }

    async login(user) {
        const iut_encrypt = require('@fiau372/iut-encrypt');
        const {User} = this.server.models();
        const Jwt = require('@hapi/jwt');
        user.password = iut_encrypt.sha1(user.password);
        try {
            let userT = await User.query().where('email', '=', user.email).andWhere('password', '=', user.password);
            let tab = Object.values(userT)[0];
            let str = JSON.stringify(tab);
            let scop = 'user';
            if (str.includes('admin')) {
                scop = 'admin';
            }
            if (userT.length > 0) {
                const token = Jwt.token.generate(
                    {
                        aud: 'urn:audience:iut',
                        iss: 'urn:issuer:iut',
                        //firstName: user.firstName,
                        //lastName: user.lastName,
                        email: user.email,
                        scope: scop
                    },
                    {
                        key: 'random_string', // La clé qui est définit dans lib/auth/strategies/jwt.js
                        algorithm: 'HS512'
                    },
                    {
                        ttlSec: 14400 // 4 hours
                    }
                );
                return '{ login: "successful" }' + 'Bearer ' + token;
            }
        } catch (e) {
            return e;
        }

        return '401 Unauthorized';
    }
}