'use strict';

module.exports = {

    async up(knex) {

        await knex.schema.createTable('mail', (table) => {

            table.increments('id').primary();
            table.string('from').notNull();
            table.string('to').notNull();
            table.string('subject').notNull();
            table.string('text').notNull();
            table.string('signature').notNull();
            table.string('date').notNull();

            table.dateTime('createdAt').notNull().defaultTo(knex.fn.now());
            table.dateTime('updatedAt').notNull().defaultTo(knex.fn.now());
        });
    },

    async down(knex) {

        await knex.schema.dropTableIfExists('mail');
    }
};
